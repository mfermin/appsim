#include "imstkTimer.h"
#include "imstkSimulationManager.h"
#include "imstkSceneManager.h"
#include "imstkFeDeformableObject.h"
#include "imstkBackwardEuler.h"
#include "imstkNonLinearSystem.h"
#include "imstkNewtonSolver.h"
#include "imstkGaussSeidel.h"
#include "imstkPlane.h"
#include "imstkTetrahedralMesh.h"
#include "imstkMeshIO.h"
#include "imstkOneToOneMap.h"   
#include "imstkAPIUtilities.h"
#include "imstkConjugateGradient.h"
#include "imstkLight.h"
#include "imstkCamera.h"
#include "imstkFEMDeformableBodyModel.h"
#include "imstkSurfaceMesh.h"
#include "imstkScene.h"
#include "imstkRigidBodyModel.h"
//#include "imstkNew.h"

//PBD
#include "imstkPbdModel.h"
#include "imstkPbdObject.h"
#include "imstkTetraTriangleMap.h"

//
#include "imstkVisualObjectImporter.h"
//#include "imstkVulkanInteractorStyle.h"
#include "imstkGeometryUtilities.h"
#include "imstkSurfaceMesh.h"


#include <memory>

using namespace imstk;
using namespace std;

std::shared_ptr<DynamicObject> 
makeFEM_Object(std::shared_ptr<Scene> scene, std::shared_ptr<PointSet> tetMesh, const Vec3d& position);

std::shared_ptr<PbdObject> 
makePBD_Object(std::shared_ptr<Scene> scene, std::shared_ptr<PointSet>  tetMesh, const Vec3d& position);

//LOG(INFO) << meshFileName;

/// \brief This script demonstrates the soft body simulation using FEM & PBD
/// using Finite elements & PBD 
/// This example have parameter of paper: "XPBD: Position-Based Simulation of Compliant Constrained Dynamics"-(Miles MacklinMatthias Müller, Nuttapong Chentane)
/// Parameters Young’s modulus E= 105, Poisson’s ratio μ= 0.3, and ∆t= 0.008.
///
int
main()
{
    // simManager and Scene
    auto simConfig = std::make_shared<SimManagerConfig>();
    simConfig->simulationMode = SimulationMode::Rendering;
    auto simManager = std::make_shared<SimulationManager>(simConfig);
    auto scene      = simManager->createNewScene("DeformableBodyFEM");

    // Load a tetrahedral mesh
    //auto tetMesh = MeshIO::read(meshFileName);

    //tetrahelized model with gmsh  **andando***
    //auto tetMesh = imstk::MeshIO::read(iMSTK_DATA_ROOT "modelMesh/obj2veg/output/mesh5x5_gmsh.vtk");
    auto tetMesh = imstk::MeshIO::read(iMSTK_DATA_ROOT "mesh/surfBar10_3d.vtk");
    auto tetMesh2 = imstk::MeshIO::read(iMSTK_DATA_ROOT "mesh/surfBar10_3d.vtk");


    //const std::string& tetMeshObj = iMSTK_DATA_ROOT "modelMesh/obj2veg/output/mesh5x5_gmsh.vtk";
    //auto mesh2d = imstk::MeshIO::read(iMSTK_DATA_ROOT "mesh/surfBar.vtk");

    // ***funciona lento***
    //auto tetMesh = std::dynamic_pointer_cast<TetrahedralMesh>(MeshIO::read(iMSTK_DATA_ROOT "armadillo/armadillo_volume.vtk"));
    //auto tetMesh = std::dynamic_pointer_cast<TetrahedralMesh>(MeshIO::read(iMSTK_DATA_ROOT "modelMesh/liver/liverRef.vtk"));

    //tetrahelized with paraview *no funciona*
    //auto tetMesh = std::dynamic_pointer_cast<TetrahedralMesh>(MeshIO::read(iMSTK_DATA_ROOT "modelMesh/cubeVTK.vtk")); 
    
    // **desde stack image**
    //auto imageData = imstk::MeshIO::read(iMSTK_DATA_ROOT "skullVolume.nrrd"); //for volume rendering

    /////

    CHECK(tetMesh != nullptr) << "Could not read mesh from file.";


    // Scene object 1: FEM object
    auto autoFEMobj = makeFEM_Object(scene, tetMesh, Vec3d(0,0,1));

    // Scene object 2: PBD object
    auto autoPBDobj = makePBD_Object(scene, tetMesh2, Vec3d(0,0,0));

    //anda!! pero no cambia o no actualiza en esta parte: inicia con 0, pasa a -9 y queda con -9 pero no simula?
    auto extFunc =
        [&autoPBDobj](Module* module)
        {
            auto s = autoPBDobj->getPbdModel()->getParameters();

            LOG(INFO) << "Parameter: " << s->m_gravity[1];
            auto confParams = std::make_shared<PBDModelConfig>();  
            confParams->m_gravity   = Vec3d(0, -9, 0);  //add force extern //-9.8
            autoPBDobj->getPbdModel()->configure(confParams);
            LOG(INFO) << "Function callback";
                
        };

    //Aca puedo agregar un modificador funcion de fuerza externa modificando parametro en PBD parameter_m->gravity_m[1] +=0.01; 
    simManager->getSceneManager(scene)->setPreUpdateCallback(extFunc); //pre and post

    // Scene object 2: Plane
    auto planeGeom = std::make_shared<Plane>();
    planeGeom->setWidth(10);
    planeGeom->setPosition(0, -4, 0);
    auto planeObj = std::make_shared<CollidingObject>("Plane");
    planeObj->setVisualGeometry(planeGeom);
    planeObj->setCollidingGeometry(planeGeom);
    scene->addSceneObject(planeObj);



    // Set Camera configuration
    auto cam = scene->getCamera();
    cam->setPosition(Vec3d(8, 0, -20));

    // Light
    auto light = std::make_shared<DirectionalLight>("light");
    light->setFocalPoint(Vec3d(-40, 40, -40));
    light->setIntensity(1.);

    auto light2 = std::make_shared<DirectionalLight>("light2");
    light2->setFocalPoint(Vec3d(10, -1, 30));
    light2->setIntensity(0.2);

    scene->addLight(light);
    scene->addLight(light2);


    // Run the simulation
    simManager->setActiveScene(scene);
    simManager->getViewer()->setBackgroundColors(Vec3d(1, 1, 1));
    //simManager->getViewer()->setBackgroundColors(Vec3d(0.89453, 0.83984, 0.73828));
    //simManager->getViewer()->setBackgroundColors(Vec3d(0.05, 0.01, 0.1), Vec3d(0.13836, 0.13836, 0.2748), true);
    
    simManager->start(SimulationStatus::Paused);

    return 0;
}
/// \brief Generate a Deformable object simulated with FEM
/// \param name of the object
/// \param position of the object
std::shared_ptr<DynamicObject>
makeFEM_Object(std::shared_ptr<Scene>    scene,
                         std::shared_ptr<PointSet> tetMesh, const Vec3d& position)
{
    auto dynTetMesh = std::dynamic_pointer_cast<TetrahedralMesh>(tetMesh);
    CHECK(dynTetMesh != nullptr) << "Dynamic pointer cast from PointSet to TetrahedralMesh failed in FEM!";

    //transform data
    tetMesh->translate(position, Geometry::TransformType::ApplyToData);
    //tetMesh->rotate(Vec3d(1.0, 0.0, 0.0), -1.3, Geometry::TransformType::ApplyToData);
    auto surfMesh = std::make_shared<SurfaceMesh>();
    //imstkNew<SurfaceMesh> surfMesh;
    dynTetMesh->extractSurfaceMesh(surfMesh, true);

    // Setup the Parameters
    auto config    = std::make_shared<FEMModelConfig>(); 
    //imstkNew<FEMModelConfig> config;
    config->m_fixedNodeIds = { 1,2,5,6 };
    //config->m_dampingMassCoefficient      = 0.1;
    //config->m_dampingStiffnessCoefficient = 0.01;
    //config->m_dampingLaplacianCoefficient = 0.05; 
    //config->m_deformationCompliance       = 1.0;
    //config->m_compressionResistance       = 500.0;
    //config->m_gravity = 9.81;

    // Setup the Model
    auto dynaModel = std::make_shared<FEMDeformableBodyModel>();
    //imstkNew<FEMDeformableBodyModel> dynaModel;
    dynaModel->configure(config);
    dynaModel->setTimeStepSizeType(TimeSteppingType::Fixed);//realTime);//Fixed);
    dynaModel->setModelGeometry(dynTetMesh);
    auto timeIntegrator = std::make_shared<BackwardEuler>(0.001);// Create and add Backward Euler time integrator
    //imstkNew<BackwardEuler> timeIntegrator(0.01);
    dynaModel->setTimeIntegrator(timeIntegrator);

    // Setup the VisualModel
    auto surfMeshModel = std::make_shared<VisualModel>(surfMesh); 
    //imstkNew<VisualModel>    surfMeshModel(surfMesh.get());
    auto material = std::make_shared<RenderMaterial>();
    //imstkNew<RenderMaterial> material;
    material->setDisplayMode(RenderMaterial::DisplayMode::Surface);
    material->setColor(Color(0, 220. / 255.0, 0)); //color GREEN
    material->setMetalness(100.f);
    material->setRoughness(0.5);
    material->setEdgeColor(Color::Teal);
    material->setAmbientLightCoeff(50.);
    material->setShadingModel(RenderMaterial::ShadingModel::Phong);
    material->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);
    surfMeshModel->setRenderMaterial(material);

    // Setup the Object
    auto objfem = std::make_shared<FeDeformableObject>("ObjectFEM");
    //imstkNew<FeDeformableObject> object(name); //other hand to create 
    objfem->addVisualModel(surfMeshModel);
    objfem->setPhysicsGeometry(dynTetMesh);
    auto oneToOneNodalMap = std::make_shared<OneToOneMap>(tetMesh, surfMesh);
    objfem->setPhysicsToVisualMap(oneToOneNodalMap);
    objfem->setDynamicalModel(dynaModel);

    scene->addSceneObject(objfem);

    return objfem;
}

std::shared_ptr<PbdObject>
makePBD_Object(std::shared_ptr<Scene> scene,
                      std::shared_ptr<PointSet> tetMesh, const Vec3d& position)
{
    // Setup the Geometry 
    auto dynTetMesh = std::dynamic_pointer_cast<TetrahedralMesh>(tetMesh);
    CHECK(dynTetMesh != nullptr) << "Dynamic pointer cast from PointSet to TetrahedralMesh failed in PBD!";
    auto surfMesh = std::make_shared<SurfaceMesh>();
    //auto dynTetMesh   = MeshIO::read<TetrahedralMesh>(tetMesh); //

    //transform data
    tetMesh->translate(position, Geometry::TransformType::ApplyToData);
    //tetMesh->rotate(Vec3d(1.0, 0.0, 0.0), -1.3, Geometry::TransformType::ApplyToData);

    //imstkNew<SurfaceMesh> coarseSurfMesh;
    dynTetMesh->extractSurfaceMesh(surfMesh, true);
    auto map = std::make_shared<TetraTriangleMap>(tetMesh, surfMesh);
    
    //
     // Setup the Parameters
    auto pbdParams = std::make_shared<PBDModelConfig>();
    //imstkNew<PBDModelConfig> pbdParams;

    //st-vk
    pbdParams->m_femParams->m_YoungModulus = 1000;//350.0;
    pbdParams->m_femParams->m_PoissonRatio = 0.3;//0.45;
    pbdParams->m_fixedNodeIds = {1,2,5,6};
    pbdParams->enableFEMConstraint(PbdConstraint::Type::FEMTet, PbdFEMConstraint::MaterialType::StVK);
    pbdParams->m_uniformMassValue = 1.0;
    pbdParams->m_gravity    = Vec3d(0, -9.8, 0);
    pbdParams->m_defaultDt  = 0.01;//0.008;
    pbdParams->m_iterations = 10;//20;
    pbdParams->collisionParams->m_proximity = 0.3;
    pbdParams->collisionParams->m_stiffness = 0.1;

    // Setup the Model
    auto model      = std::make_shared<PbdModel>();
    //imstkNew<PbdModel> model;
    
    model->setDefaultTimeStep(0.02);
    model->setTimeStepSizeType(imstk::TimeSteppingType::Fixed);
    model->setModelGeometry(tetMesh);
    model->configure(pbdParams);


    // Setup the VisualModel
    auto material = std::make_shared<RenderMaterial>();
    //imstkNew<RenderMaterial> material;
    material->setDisplayMode(RenderMaterial::DisplayMode::Surface);
    material->setColor(Color(220. / 255.0, 0, 0)); //color RED
    material->setMetalness(100.f);
    material->setRoughness(0.5);
    material->setEdgeColor(Color::Teal);
    material->setAmbientLightCoeff(50.);
    material->setShadingModel(RenderMaterial::ShadingModel::Phong);
    material->setDisplayMode(RenderMaterial::DisplayMode::WireframeSurface);

    auto surfMeshModel = std::make_shared<VisualModel>(surfMesh);
    //imstkNew<VisualModel> surfMeshModel(highResSurfMesh);
    surfMeshModel->setRenderMaterial(material);

    auto pbdObj = std::make_shared<PbdObject>("ObjectPBD");
    //imstkNew<PbdObject> pbdObj(name);

    // Setup the Object
    pbdObj->setDynamicalModel(model);
    pbdObj->addVisualModel(surfMeshModel);
   //pbdObj->setCollidingGeometry(coarseSurfMesh);
    pbdObj->setPhysicsGeometry(tetMesh);
    pbdObj->setPhysicsToVisualMap(map);


    scene->addSceneObject(pbdObj);

    return pbdObj;
}
