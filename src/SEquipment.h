
#include "imstkTimer.h"
#include "imstkSimulationManager.h"
#include "imstkSceneManager.h"
#include "imstkLight.h"
#include "imstkCamera.h"
#include "imstkScene.h"
#include "imstkVisualObjectImporter.h"
#include "imstkCollidingObject.h"
//#include "sceneObject.h"

#include <memory>

using namespace imstk;
using namespace std;


class SEquipment : public CollidingObject
{
private:

   //vtkSmartPointer<vtkPolyData> object;

   double scale;
   Vec3d center;
   double rot[3];

   void buildGrip();
   bool activedCube;
   int index;

   //vector<pair<Vec3d, Vec3d>> instrument[3];
   //std::vector<SurfaceMesh::TriangleArray> meshes[3];

	Vec3d gridPosition = Vec3d(0, 0., 0.);
	float gradeOpen=0.2;
	std::shared_ptr<Scene>m_scene;
	//std::shared_ptr<CollidingObject> instrument1; 
	std::shared_ptr<CollidingObject> instrument1 = std::shared_ptr<CollidingObject>(); 
	//std::string& m_name;

public:
	/*explicit SEquipment(const std::string& name) : CollidingObject(name)
    {
        m_type = Type::Colliding;
    }*/
    //constructor
    explicit SEquipment(const std::string& name,
                std::shared_ptr<Scene> &scene //->this scene
                ): CollidingObject(name){
    	m_type = Type::Colliding;
        this->m_scene = scene;

        auto meshGripCy1= imstk::MeshIO::read(iMSTK_DATA_ROOT "equipment/cylinder1.obj"); //faltan las otras geometrias.
	    auto surfaceGripCy1 = std::dynamic_pointer_cast<imstk::SurfaceMesh>(meshGripCy1);

	    surfaceGripCy1->translate(gridPosition, Geometry::TransformType::ApplyToData);
	    //instrument1 =std::shared_ptr<CollidingObject>();
	     
	   //this->instrument1= std::make_shared<CollidingObject>("I1");
	    this->instrument1->setVisualGeometry(surfaceGripCy1);
	    this->instrument1->setCollidingGeometry(surfaceGripCy1);
	    //m_scene->addSceneObject(instrument1);


        }
	enum model{
		holder,
		pivot,
		up, down,
		};

	void addModelSegment(int model, Vec3d a, Vec3d b, float radius){
		a = Vec3d(a[0], a[2], -a[1]);
		b = Vec3d(b[0], b[2], -b[1]);
		//instrument[model].push_back(make_pair(a, b));
	};

	bool initialize(){

		if (!SceneObject::initialize())
    {
        return false;
    }

    if (m_collidingToVisualMap)
    {
        m_collidingToVisualMap->initialize();

        
	   //m_scene->addSceneObject(instrument1);
    }
    return true;
    
	};
	std::shared_ptr<CollidingObject>
	getInstrument(){
		return instrument1;

	};

	Vec3d getPosition(){
		//return (Vec3d)currentlyPositiion[index];
		Vec3d Position(0,0,0);
		return Position;
	}
    //vtkSmartPointer<vtkActor>  translation(int operation);
    //vtkint index;int index;SmartPointer<vtkActor>  rotation(int operation);

};