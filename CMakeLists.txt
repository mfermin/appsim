cmake_minimum_required (VERSION 3.10)
# project
project (Appsim)

#mauro
set(iMSTK_DIR "/home/mauro/devSim/iMSTK/Release/lib/cmake/iMSTK-3.0")
# iMSTK data
add_definitions(-DiMSTK_DATA_ROOT="${CMAKE_SOURCE_DIR}/data/")
#renex
#set(iMSTK_DIR "/home/simanato/workspace/iMSTKBox/iMSTK-dev/build/install/lib/cmake/iMSTK-3.0")
find_package(iMSTK REQUIRED)

#debug
message(STATUS "iMSTK: ${iMSTK_FOUND}")
message(STATUS "iMSTK headers: ${iMSTK_INCLUDES}")
message(STATUS "iMSTK libs: ${iMSTK_LIBRARIES}")

list (APPEND CMAKE_MODULE_PATH "${${PROJECT_NAME}_SOURCE_DIR}")


# Demo source code & headers
set(DemoSources
    ${CMAKE_SOURCE_DIR}/src/main.cpp
)
set(DemoHeaders
    #Add here headers
)

#find_package(OpenGL REQUIRED)
#find_package(glfw3 REQUIRED)




# headers
include_directories(
       ${DemoHeaders}
       ${iMSTK_INCLUDES}
       #${GLFW3_INCLUDE_DIR}
       #${OPENGL_INCLUDE_DIRS}
       #${GLAD_INCLUDE_DIRS}
)

#link_libraries(${GLFW_LIBRARY_DIRS})

# generate binary file
add_executable(App ${DemoSources})

target_include_directories (App
  PUBLIC
    "${${PROJECT_NAME}_SOURCE_DIR}"
)

#link binary with libs, comment the not used libs
target_link_libraries(App PUBLIC
       imstk::Common
       imstk::Geometry
       imstk::DataStructures
       imstk::Devices
       imstk::Materials
       imstk::Rendering
       imstk::Solvers
       imstk::DynamicalModels
       imstk::CollisionDetection
       imstk::CollisionHandling
       imstk::SceneEntities
       imstk::Scene
       imstk::SimulationManager
       imstk::Constraints
       imstk::Animation
       imstk::apiUtilities
       #OpenGL::GL
       #glfw
)
